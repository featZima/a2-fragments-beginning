package com.example.featzima.testapplication;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class FragmentsAdapter extends FragmentStatePagerAdapter {

    public FragmentsAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        String title = "";
        if (i == 0) {
            title = "Friends";
        }
        else if (i == 1) {
            title = "Photos";
        }
        else if (i == 2) {
            title = "Settings";
        }
        else throw new RuntimeException("Unknown fragment index");
        return A2Fragment.createInstance(title);
    }

    @Override
    public int getCount() {
        return 3;
    }
}
