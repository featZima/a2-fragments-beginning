package com.example.featzima.testapplication;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class A2Fragment extends Fragment {

    protected TextView testTextView;
    protected TitleOwner titleOwner;

    public String getTitle() {
        return getArguments().getString(ARG_TITLE);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        titleOwner = (TitleOwner) context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        titleOwner = (TitleOwner) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        titleOwner = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.actionClose) {
            getActivity().finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View fragmentView =  inflater.inflate(R.layout.fragment_a2, container, false);
        testTextView = fragmentView.findViewById(R.id.testTextView);
        testTextView.setText(getTitle());
        return fragmentView;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (titleOwner != null) {
            titleOwner.setTitle(getTitle());
        }
    }

    private final static String ARG_TITLE = "argTitle";

    public static A2Fragment createInstance(String title) {
        A2Fragment fragment = new A2Fragment();
        Bundle arguments = new Bundle();
        arguments.putString(ARG_TITLE, title);
        fragment.setArguments(arguments);
        return fragment;
    }
}
